FROM pipeline_petclinic

WORKDIR /app
ADD target/*.java /app/

CMD java -jar /app/*.jar


