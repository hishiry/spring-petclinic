pipeline {
    agent {
      label 'web'
    }

    tools {
      allure 'allure'
      git 'Default'
      maven 'Default'
    }

    // tools {
    //     // Install the Maven version configured as "M3" and add it to the path.
    //     maven "M3"
    // }

    stages {
        stage('Clean') {
            steps {

            }
        }
        stage('Build') {
            steps {
                git branch: 'dev', url: 'https://gitlab.com/hishiry/spring-petclinic.git'
                sh "mvn -Dcheckstyle.skip=true -Dmaven.test.failure.ignore=true -Dmaven.test.skip=true package"

            }
            post {
                success {
                    //存档
                    archiveArtifacts 'target/*.jar'
                }
            }
        }
        stage('Tests') {
            parallel {
                stage('UnitTest') {
                    steps {
//                        sh 'rm -rf allure junit.xml'
                        sh "mvn -Dcheckstyle.skip=true -Dmaven.test.failure.ignore=true test"
                    }
                    post {
                        success {
                            junit 'target/surefire-reports/TEST*.xml'
                            //存档
                            archiveArtifacts 'junit.xml'
        //                    allure includeProperties: false, jdk: '', results: [[path: 'allure']]
                        }
                    }
                }
                stage('StaticTesting') {
                    steps {
                        sh "echo mvn sonar:sonar"

                    }
                }
            }
        }
        stage("release"){
            steps{
                sh 'echo maven publish'
                sh 'echo docker image build'
            }
        }

    }
}
